package com.chen66.service;

import com.chen66.pojo.Books;

import java.util.List;

//BookService:底下需要去实现,调用dao层
public interface BookService {
    //增加书籍
    int addBook(Books books);
    //删除书籍,通过Id删除
    int deleteBook( int id);
    //修改书籍
    int updateBook(Books books);
    //查找书籍
    //通过Id查找
    Books queryBookById(int id);
    //查找所有书籍
    List<Books> queryAllBooks();
}
