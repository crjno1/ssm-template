package com.chen66.dao;

import com.chen66.pojo.Books;
import org.apache.ibatis.annotations.Param;

import java.util.List;


public interface BookMapper {
    //增加书籍
    int addBook(Books books);
    //删除书籍,通过Id删除
    int deleteBook(@Param("BookId") int id);
    //修改书籍
    int updateBook(Books books);
    //查找书籍
    //通过Id查找
    Books queryBookById(@Param("BookId") int id);
    //查找所有书籍
    List<Books> queryAllBooks();
}
